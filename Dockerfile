ARG pg_version=13.10
FROM public.ecr.aws/docker/library/postgres:${pg_version}
RUN export DEBIAN_FRONTEND=noninteractive; apt-get update && apt-get install -y build-essential